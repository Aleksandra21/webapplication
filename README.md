# Bio-informatics workshop website

The purpose of this workshop is to give future bio-informatics students an general idea of what to expect from the study      
by guiding them through a tutorial that's an example based on a first year subject.

### Prerequisites

* IntelliJ IDEA 2018.2.5(Ultimate Edition)
* OpenJDK 10.0.2
* SDK: Java version 10.0.2
* Command Prompt

### Installing

The following steps will tell you how to get this development environment running.

Clone the given repository in to your local machine using IntelliJ or the command line. [Clone a repository](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html)
```
git clone https://marjan5sj@bitbucket.org/Aleksandra21/researchwebapplication.git
```
* You may need to enter your Bitbucket credentials
* Import the build.gradle, you can find the build.gradle in researchwebapplication --> web.templates --> bioinf.workshop (follow from "Importing a project from a Gradle model" of this 
    [help page](https://www.jetbrains.com/help/idea/importing-a-gradle-project-or-a-gradle-module.html?search=import%20gradle))
* You may need to configure the JDK, via the Project Structure dialog  
    - File --> Project Structure --> SDKs (use /usr/local/jdk1.8.0_121)
* After doing the steps mentioned above, Intellij will install all dependencies that are needed for this program to run.

### Running the program
* After following the Installment steps, go to: 
    - Intelij: 
    - This projects main file is: textInformation.jsp (note that you have to add a Tomcat configuration first.)
    - After running the jsp, the website will show up, then you need to:
        - click on the title(What is bio-informatica?)
        - click on the title(De missie!)
        - Click then on the buttons 
        - When the scientist-man pops up click on the speech-bubble.
        - Fill in the Assignment (You can find the answers in the directory: txtFiles/assignmentAnswers.txt )
        - After submitting, copy the sequence and click on the link to go to the database webpage.
        - paste the sequence and click to load the structure.
        - Answer the questions.(You can find the answers in the directory: txtFiles/assignment.txt)
        - YOU ARE DONE NOW!
        
## Authors

* **Alexandra Kapielska & Marjan Shirzai**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
