package nl.bioinf.marjan.aleksandra.webapplication;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "match", urlPatterns = "/match")
public class match extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //getting sequence
        String seq = request.getParameter("sequence");
        seq = seq.replaceAll("\"", "");

        //Makes the sequence uppercase
        String uppercaseCheck = NucleotideSequenceDatabase.INSTANCE.toUppercase(seq);
        String bacterium = NucleotideSequenceDatabase.INSTANCE.getBacterium(uppercaseCheck);

        response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(bacterium);       // Write response body.
        PrintWriter output = response.getWriter();
        System.out.println(bacterium);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}


