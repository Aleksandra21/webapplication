package nl.bioinf.marjan.aleksandra.webapplication;

import java.util.HashMap;
import java.util.Map;

/**
 * The java class NucleotideSequenceDatabase checks if the given key exists and returns the value to the user.
 */

public class NucleotideSequenceDatabase {

    //Declaring the hashmap to store the data in key value pairs.
    private Map<String, String> hmap = new HashMap<>();

    //Creating the instances only one time
    public static final NucleotideSequenceDatabase INSTANCE = new NucleotideSequenceDatabase();

    private NucleotideSequenceDatabase() {
        hmap.put(">ATCC", "Chaparonen");
        hmap.put("DDPPATVYRYDSRPPEDVFQNGFTAWGNNDNVLDHLTGRSCQVGSSNSAFVSTSSSRRYTEVYLEHRMQEAVEAERAGRGTGHFIGYIYEVRADNNFYGAASSYFEYVDTYGDNAGRILAGALATYQSEYLAHRRIPPENIRRVTRVYHNGITGETTTTEYSNARYVSQQTRANPNPYTSRRSVASIVGTLVRMAPVIGACMARQAESSEAMAAWSERAGEAMVLVYYESIAYSF", "Click on me to load the structure");
        hmap.put("CCCCTTTT", "Thermoactinomyces");
    }

    //The input is made uppercase
    public String toUppercase(String seqUpper){
        return seqUpper.toUpperCase();
    }

    public String getBacterium(String seq){
        //Adding elements to HashMap
        String bacteriaName = "";

        //Checks if key exists in HashMap
        if (hmap.containsKey(seq.toUpperCase())) {
            //key exists
            bacteriaName += hmap.get(seq);
        }
        else {
            //key does not exists
            bacteriaName += "Oeps! Nothing found in the database, please try again.";
        }
        return bacteriaName;

    }
}

