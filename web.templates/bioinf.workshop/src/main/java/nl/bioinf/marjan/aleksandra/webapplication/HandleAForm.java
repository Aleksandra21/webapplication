package nl.bioinf.marjan.aleksandra.webapplication;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "HandleFormDemoServlet", urlPatterns = "/handle.form")
public class HandleAForm extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<String> classes = new ArrayList<>();
        classes.add("1");
        classes.add("2");
        classes.add("3");
        classes.add("4");

        request.setAttribute("classes", classes);
        final RequestDispatcher requestDispatcher = request.getRequestDispatcher("progAssignment.jsp");
        requestDispatcher.forward(request, response);
        }}
