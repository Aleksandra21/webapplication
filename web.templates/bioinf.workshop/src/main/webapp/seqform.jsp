<%--
  Created by IntelliJ IDEA.
  User: Marjan Shirzai & Alexandra Kapielska
  Date: 12-12-18
  Time: 9:20
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


<html>
<head>

    <meta http-equiv="content-type" content="text/html"; charset="ISO-8859-1">
    <title></title>
    <link rel="stylesheet" href="/css/thema10.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type = "text/javascript" src="js/fastavalidator.js"></script>
    <script type = "text/javascript" src="js/secondPageJquery.js"></script>

    <%--JavaScript and css for protein structure --%>
    <link rel="stylesheet" href="https://www.ncbi.nlm.nih.gov/Structure/icn3d/lib/jquery-ui.min.css">
    <link rel="stylesheet" href="https://www.ncbi.nlm.nih.gov/Structure/icn3d/icn3d_simple_ui.css">
    <script src="https://www.ncbi.nlm.nih.gov/Structure/icn3d/lib/jquery.min.js"></script>
    <script src="https://www.ncbi.nlm.nih.gov/Structure/icn3d/lib/jquery-ui.min.js"></script>
    <script src="https://www.ncbi.nlm.nih.gov/Structure/icn3d/lib/three.min.js"></script>
    <script src="https://www.ncbi.nlm.nih.gov/Structure/icn3d/simple_ui_all.min.js"></script>
    <script type="text/javascript" src="js/secondPageJquery.js"></script>
    <script type = "text/javascript" src="js/formvalidator.js"></script>

    <%-- stylesheet for loader --%>
    <link rel="stylesheet" href="css/loader.css">

</head>
<body>

<h1> sequentie analyse</h1>
<div>
</div>
<iframe width="0" height="0" border="0" name="dummyframe" id="dummyframe"></iframe>

<form id="match" target="dummyframe" onsubmit="validateFasta(this)">
  <h2>Plak hier je sequentie om de proteïne structuur te laden:</h2><br>
  <input id="fasta1" type="text" name="sequence">
  <input type="submit" value="Submit" id="submit-btn">

    <%-- receives the database results --%>
    <div id="organism" name="bact"></div>

   <br/>
    <div id="loader"></div>

</form>

<%-- receives the protein structure --%>
<div id="icn3dwrap"></div>



<div id="proteinAssignment" class="w3-third">


    <jsp:include page="ProteinAssignment.jsp" />
</div>



<div id="finalAnswer" class="w3-third">


    <jsp:include page="finalAnswer.jsp" />
</div>

</body>
</html>