<%--
  Created by IntelliJ IDEA.
  User: aleksandra
  Date: 28/01/2019
  Time: 18:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Eiwit structuur analyse</title>
    <script type = "text/javascript" src="js/formvalidator.js"></script>
</head>
<body>
<form id= "protein" name = "proteinAssign" onsubmit="validateAssignment();return false">
<h4>Hoeveel subunits heeft het eiwit?</h4>
<select id="subunits" class="required">
    <option value = "1">1</option>
    <option value = "2">5</option>
    <option value = "3">8</option>
    <option value = "4">12</option>
</select>

    <h4>Bevat het eiwit alfa helices?</h4>
    <select id="alfa" class="required">
        <option value = "1">Ja</option>
        <option value = "2">Nee</option>
    </select>

    <h4>En Bèta-sheets ?</h4>
    <select id="beta" class="required">
        <option value = "1">Ja</option>
        <option value = "2">Nee</option>
    </select>

    <button type="submit" id="pbutton">Ga verder</button>
</form>


</body>
</html>
