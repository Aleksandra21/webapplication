<%--
  Created by IntelliJ IDEA.
  User: Marjan Shirzai & Alexandra Kapielska
  Date: 9-1-19
  Time: 9:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Bio-informatica tekst</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <%-- Webpage stylesheet --%>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <%-- speechbubble stylesheet --%>
    <link rel="stylesheet" href="css/bubble.css">

    <script type = "text/javascript" src="js/firstPageFunctions.js"></script>
    <script src="js/jquery.hugo-decoded.typewriter.min.js"></script>

</head>
<body>

<header class="w3-container w3-Blue w3-center" style="padding:128px 16px">
<h1 class="w3-margin w3-jumbo" id="intro">Wat is bio-informatica?</h1>

    <%-- Assigning the text information with a id --%>
<div id="introtxt">&nbsp;</div>
</header>

<%-- Assigning the workshop text and title in the middel --%>
<div class="w3-container w3-white w3-center w3-opacity w3-padding-64">
<h1 class="w3-margin w3-xlarge" id="workshop" >De missie!</h1>
<div id="workshoptxt">&nbsp;</div>

    <%-- created a next button --%>
    <input id="next" type="submit" value="Haal de sequentie op!">
    <br>
    <div id="seq">&nbsp;</div>
    <br>
    <input id="verder" type="submit" value="ga verder!">
</div>

<%--Created a speechbubble --%>
<div class="speech-bubble" id="bubble">&nbsp;
    <div class="arrow bottom right"></div>
</div>

<%-- Added the python addignment --%>
<div id="progAssignment" class="w3-third">
    <jsp:include page="progAssignment.jsp" />
</div>

<%-- Assigning the type writer with a id --%>
<div id="typewriter"></div>

<br style="line-height: 4;">

<%-- Scientist man image added --%>
<div class="w3-margin w3-xlarge">
    <img  id="scientist" src="imgs/man-scientist.png" width="400" height="400">
</div>

<br style="line-height: 2;">
<%-- Link to the second webpage --%>
<div> <a class="seqform" href="seqform.jsp">Let's go to the database!</a> </div>
</body>
</html>