<%--
  Created by IntelliJ IDEA.
  User: akapielska
  Date: 7-1-19
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <script type = "text/javascript" src="js/formvalidator.js"></script>

</head>
<body>
<iframe width="0" height="0" border="0" name="dummyframe" id="dummyframe"></iframe>
<form id= "prog" name = "progAssign"  target="dummyframe" method="get" onsubmit="return validateForm()">
        <h4>Programmeer opdracht </h4>
        <p>
            <h4>Openen van bestand met sequentie</h4>
            <p>sequentie = open(seq.txt")</p>

            <h4>Door het bestand heen lopen per lijn (line)</h4>
            <label>for</label>
            <input id="answer1" type="text" name="answer1">
    <label>in</label>

       <input id="answer2" type="text" name="answer2">
    <h4>:</h4>

    <h4>Sequentie zonder header krijgen</h4>
    <label>if</label>
       <select id = "answer3" class="required">
            <option value = "1">-</option>
            <option value = "2">.</option>
            <option value = "3">></option>
            <option value = "4">:</option>
        </select>

           <label>in</label>
               <select id = "answer4" class="required">
                   <option value = "1">line</option>
                   <option value = "2">sequentie</option>
                   <option value = "3">header</option>
                   <option value = "4">line[0]</option>
               </select>
               <label>:</label>
               <br><br/>
               <label>seq =</label>
               <select id="answer5" class="" >
                   <option value = "1">line - header</option>
                   <option value = "2">sequentie - header</option>
                   <option value = "3">header - line</option>
                   <option value = "4">sequentie - line</option>
               </select>

               <h4>Maak van alle sequentie letters hoofdletters</h4>
               <select id="answer6" class="required">
                   <option value = "1">seq.lower()</option>
                   <option value = "2">seq.split()</option>
                   <option value = "3">seq.upper()</option>
                   <option value = "4">seq.join()</option>
               </select>

               <h4>Haal de spaties weg</h4>
               <input id="answer7" type="text" class="required" name="answer7">
               <select id="answer8" class="required">
                   <option value = "1">.sum()</option>
                   <option value = "2">.strip(' ')</option>
                   <option value = "3">.len()</option>
                   <option value = "4">.upper()</option>
               </select>
               <h4>Print de sequentie</h4>
                   <input id = answer9" type="text" class="required" name="answer9">

        </p>


    <input class = "progClicker"id ="oneclick" type="submit" value="Run your code" name="Submit" />

</form>

</body>
</html>
