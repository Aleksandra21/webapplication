$( document ).ready(function() {
    //Hides the features by giving their id.
    $("#workshop").hide();
    $("#bubble").hide();
    $("#bubble").fadeIn(1000);
    $(".speech-bubble").hide();
    $("#next").hide();
    $("#scientist").hide();
    $("#progAssignment").hide();
    $(".seqform").hide();
    $("#seq").hide();
    $("#verder").hide();

    //titel color changes when mouseover.
    $("#intro").hover(function () {
        $(this).css("color", "black");
    }, function () {
        $(this).css("color", "white");
    });

    $("#workshop").hover(function () {
        $(this).css("color", "green");
    }, function () {
        $(this).css("color", "purple");
    });

    //Loads the the externel text file.
    $("#introtxt").load("txtFiles/introduction.txt");
    $("#introtxt").hide();
    $("#intro").click(function () {
        $("#introtxt").toggle(300, function () {
            $("#workshop").fadeIn(300);
        });
    });

    $("#workshoptxt").load("txtFiles/WorkshopInformation.txt");
    $("#workshoptxt").hide();
    $("#workshop").click(function () {
        $("#workshoptxt").toggle(300, function () {
            $("#next").fadeIn(100);
        });
    });
    $("#seq").load("txtFiles/sequence.txt");

    $("#next").click(function () {
       $("#next").fadeOut(function () {
           $("#seq").css("color", "blue").fadeIn(function () {
               $("#verder").fadeIn();
           });
       });
    });

    //Loads text file to the speechbubble.
    $("#bubble").load("txtFiles/start.txt");

    // this piece of code is not efficient, but due to time constraints, it has been done in this way.
    //Loads the typeWriter
    $("#verder").click(function () {
        $("#verder").fadeOut();
        $("#progAssignment").fadeIn(function () {
           $("#scientist").fadeIn(function () {
              $("#bubble").fadeIn().click(function () {
                  $("#bubble").load("txtFiles/txtbubble1.txt").click(function () {
                      $("#scientist").fadeOut();
                      $("#bubble").fadeOut(function () {
                          $('#typewriter').typewrite( {
                                  text: [ "We beginnen met het inladen van onze sequentie:",
                                      "Sequentie = open(“sequentie.txt”)", "Daarna gebruiken we een for loop om door het bestand heen te stappen.",
                                      "Een loop is een herhaling of lus en is bedoeld voor het herhalen van stukjes code." , "Later gaan we ook nog een if statement gebruiken.\n" +
                                      "If betekent; als en wordt gebruikt om bepaalde condities met elkaar te vergelijken.\n" +
                                      "Bijvoorbeeld:\n" +
                                      "If sequentie.length() > 0:\n" +
                                      "Hier staat dus als de lengte van de sequentie groter dan nul is.\n" +
                                      "Als de conditie waar is dan wordt alle code die onder de statement staat, uitgevoerd.", "Ook heeft Python verschillende ingebouwde functies, één van de belangrijkste functie is de print() functie.\n" +
                                      "Deze print alles wat je tussen de haakjes opgeeft. Er zijn nog veel meer ingebouwde functies die je tegen zult komen.\n" +
                                      "Als je niet weet wat deze functies doen dan kun je ze altijd nog googelen.", "Succes met jou eerste programmeer opdracht!"],
                                  speed: 10
                              }
                          );
                      });
                  });
              });
           });
        });
    });

    //Returns a link after clicking on the submit button.
    $(".progClicker").click(function () {
        $(".seqform").fadeIn();
    });
});