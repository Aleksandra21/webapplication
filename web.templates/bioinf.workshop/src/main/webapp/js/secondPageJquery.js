$( document ).ready(function() {
    //Hides the features by giving their id.
    $("#proteinAssignment").hide();
    $("#icn3dwrap").hide();
    $("#loader").hide();
    $("#finalAnswer").hide();

    //protein structure characteristics
    var cfg = {
        divid: 'icn3dwrap',
        width: 400,
        height: 400,
        resize: false,
        rotate: 'right',
        showmenu: false,
        pdbid: '1PRT',
        showtitle: false
    };

    var icn3dui = new iCn3DUI(cfg);
    icn3dui.show3DStructure();

    //Loads the loader and the protein structure.
    $("#organism").click(function () {
       $("#loader").fadeIn(function () {
           $("#loader").fadeOut(4000, function () {
              $("#icn3dwrap").fadeIn(500, function () {
                  $("#proteinAssignment").fadeIn(500);
                  $("#finalAnswer").fadeIn(500);
              });
           });
       });
    });

});