package webapplication.servlet;

import webapplication.servlet.java.coding.NucleotideSequenceDatabase;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "match", urlPatterns = "/match")
public class match extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String seq = request.getParameter("databasematcher");
        String uppercaseCheck = NucleotideSequenceDatabase.INSTANCE.toUppercase(seq);
        String bacterium = NucleotideSequenceDatabase.INSTANCE.getBacterium(uppercaseCheck);
        PrintWriter output = response.getWriter();
        output.print(bacterium);

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
